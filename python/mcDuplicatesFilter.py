import gzip
import json
import os

class mcDuplicatesFilterRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        year = kwargs.pop("year")
        runPeriod = kwargs.pop("runPeriod")
        dataset = kwargs.pop("dataset")
        inputfile = kwargs.pop("inputfile").split('//')[2]
        self.filterStr = ""

        if self.isMC:
            basepath = "{}/{}/src/Base/Filters/data/MCduplicates/".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))

            with gzip.open(f"{basepath}/duplicates_mc_nano_Run3_{year}_{runPeriod}.json.gz",'rt') as inputJson:
                duplicatesfile = json.load(inputJson)

            def get_lumiblock_to_filter(dataset):
                lumiBlocks = []
                if dataset in duplicatesfile.keys():
                    subjson = duplicatesfile[dataset]["1"]
                    for lumiBlock in subjson: # check all luminosityBlocks
                        if inputfile in subjson[lumiBlock]: # check if the file is present
                            if subjson[lumiBlock].index(inputfile) != 0: # keep first instance and filter out duplicates
                                lumiBlocks.append(lumiBlock)
                return lumiBlocks
            if isinstance(dataset, list):
                lumiBlockToFilter = []
                for d in dataset:
                    lumiBlockToFilter += get_lumiblock_to_filter(d)
            else:
                lumiBlockToFilter = get_lumiblock_to_filter(dataset)
            
            if len(lumiBlockToFilter) > 0:
              for lumi in lumiBlockToFilter:
                  self.filterStr += f"luminosityBlock != {lumi} && "
              self.filterStr = self.filterStr[:-4] # remove trailing &&

    def run(self, df):
        if self.isMC and self.filterStr != "":
            df = df.Filter(self.filterStr)
        
        return df, []


def mcDuplicatesFilterRDF(*args, **kwargs):
    """
    Filters the duplicated luminosity block from mc ntuples using
    modified HLepRare json files

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: lumiFilterRDF
            path: Base.Modules.lumiFilter
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
                dataset: self.dataset.name
                inputfile: self.get_path(self.get_input())[0]
    """
    
    return lambda: mcDuplicatesFilterRDFProducer(*args, **kwargs)
