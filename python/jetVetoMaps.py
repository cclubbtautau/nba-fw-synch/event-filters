import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Base/Filters/python/jetVetoMaps.yaml' % 
                                    os.environ['CMSSW_BASE'])

class vetoMapRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.apply_veto = kwargs.pop("apply_veto")
        self.after_jec = kwargs.pop("after_jec")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.year < 2022:
            raise ValueError("Only needed for Run3 datasets")

        if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
            ROOT.gInterpreter.Load("libBaseModules.so")

        base = "{}/src/Base/Modules".format(os.getenv("CMSSW_BASE"))
        if not ROOT.gInterpreter.IsLoaded("{}/interface/correctionWrapper.h".format(base)):
            ROOT.gROOT.ProcessLine(".L {}/interface/correctionWrapper.h".format(base))

        if not os.getenv("_vetomap"):
            os.environ["_vetomap"] = "_vetomap"

            ROOT.gInterpreter.ProcessLine(
                'auto vetomap = MyCorrections("%s", "%s");' %
                    (corrCfg[self.corrKey]["fileName"], corrCfg[self.corrKey]["corrName"]))

            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<float>&;
                using Vint = const ROOT::RVec<int>&;
                bool get_veto_flag(
                    std::string type, Vfloat eta, Vfloat phi, Vfloat pt,
                    Vint jetId, Vfloat jetChEMfrac, Vfloat jetNeEMfrac, Vint muonMatch) {
                    Double_t flag = 0;
                    for (size_t i = 0; i < pt.size(); i++) {
                        if (pt[i] > 15. && (jetId[i]==2 || jetId[i]==6) && 
                            jetChEMfrac[i]<0.9 && jetNeEMfrac[i]<0.9 && muonMatch[i]==-1) { 
                            // manual correction of phi exceeding pi to avoid SegFault
                            if (phi[i] <= -3.141592653589793) {
                                flag += vetomap.eval({type, eta[i], -3.141592653589792});
                            }
                            else if (phi[i] >= 3.141592653589793) {
                                flag += vetomap.eval({type, eta[i], 3.141592653589792});
                            }
                            else flag += vetomap.eval({type, eta[i], phi[i]});
                        }

                        // as soon as a veto jet is found just break and return flag
                        if (flag > 0) break;
                    }
                    
                    if (flag > 0) return true;
                    else          return false;
                }
            """)

    def run(self, df):
        whichJetPt = "Jet_pt"
        if self.after_jec: whichJetPt = "Jet_pt_corr"

        df = df.Define("jetMapVetoedEvent", 
                       """get_veto_flag("jetvetomap_all", Jet_eta, Jet_phi, %s,
                            Jet_jetId, Jet_chEmEF, Jet_neEmEF, Jet_muonIdx1)""" % whichJetPt)
        branches = []
        branches.append("jetMapVetoedEvent")

        if self.apply_veto:
            df = df.Filter("jetMapVetoedEvent < 1")

        return df, branches

def vetoMapRDF(**kwargs):
    """
    Module to apply veto maps that veto out events with important jets in the "hot" or 
    "cold" zones. These maps should be applied similarly both on Data and MC, to keep the
    phase-spaces equal.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jecProviderRDF
            path: Base.Modules.jec
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
                apply_veto: True/False
                after_jec: True/False
    """

    return lambda: vetoMapRDFProducer(**kwargs)
