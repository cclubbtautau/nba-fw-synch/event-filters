import ROOT
import json
import gzip
import os

for period,name in zip(["2022","2022EE", "2023", "2023BPix"], ["_preEE","_postEE", "_preBPix", "_postBPix"]):
    print(f"Running for period {period}")

    with gzip.open(f"./duplicates_mc_Run3_{period}.json.gz") as inputJson:
        jsonMiniAOD = json.load(inputJson)

    # copy structure of the json
    jsonNanoAOD = {
        k: {
            sub_k: {sub_sub_k: [] for sub_sub_k in sub_v} 
            for sub_k, sub_v in v.items()
        }
        for k, v in jsonMiniAOD.items()
    }

    for sample in jsonMiniAOD:
        print(f"    Doing sample : {sample}")
        childrenSamples = os.popen(f'dasgoclient --query="child dataset={sample}"').read()
        childrenSamples = childrenSamples.split("\n")
        globalTag = '-'.join(sample.split('/')[2].split('-')[1:])
        directChildrenSamples = [child for child in childrenSamples if globalTag in child]
        if len(directChildrenSamples) > 1:
            print(f"** WARNING: more then one NanoAOD child found for {sample}!!")
            exit()

        tmp_files_dict = {}
        tmp_runs_dict = {}
        for lumiBlock in jsonMiniAOD[sample]["1"]:
            for file in jsonMiniAOD[sample]["1"][lumiBlock]:
                if file not in tmp_files_dict.keys():
                    print(f"        - checking file : {file}")
                    childrenFiles = os.popen(f'dasgoclient --query="child file={file}"').read()
                    childrenFiles = childrenFiles.split("\n")
                    tmp_files_dict[file] = childrenFiles
                else:
                    childrenFiles = tmp_files_dict[file]

                toCheck = "NANOAODSIM/"+file.split('/')[6]
                directChildrenFiles = [child for child in childrenFiles if toCheck in child]

                for directChild in directChildrenFiles:
                    if directChild not in tmp_runs_dict.keys():
                        print(f"            - checking lumis of file : {directChild}")
                        lumis = os.popen(f'dasgoclient --query="lumi file={directChild}"').read()
                        lumis = lumis.split("\n")
                        tmp_runs_dict[directChild] = lumis
                    else:
                        lumis = tmp_runs_dict[directChild]
                    
                    if lumiBlock in lumis:
                        if directChild not in jsonNanoAOD[sample]["1"][lumiBlock]:
                            jsonNanoAOD[sample]["1"][lumiBlock].append(directChild)

        nanoSample = directChildrenSamples[0]
        jsonNanoAOD[nanoSample] = jsonNanoAOD.pop(sample)

    with open(f"duplicates_mc_nano_Run3_{period[:4]}{name}.json", "w") as outputJson:
        json.dump(jsonNanoAOD, outputJson, indent=2)

    os.system(f"gzip duplicates_mc_nano_Run3_{period[:4]}{name}.json")